﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP3
{
    public abstract class Meuble
    {
        protected uint largeur { get; set; }
        protected uint longueur { get; set; }
        protected uint hauteur { get; set; }

        public abstract void NoticeDeMontage();

        public override string ToString()
        {
            return $"{GetType().Name} de largeur {largeur}cm, de longueur {longueur}cm et de hauteur {hauteur}cm\n";
        }

    }
}
