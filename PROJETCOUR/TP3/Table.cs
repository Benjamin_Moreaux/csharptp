﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP3
{
    public class Table : Meuble
    {
        public Table(uint largeur, uint longueur, uint hauteur)
        {
            this.largeur = largeur;
            this.longueur = longueur;
            this.hauteur = hauteur;
        }

        public override void NoticeDeMontage()
        {
            Console.WriteLine("Mettre les 4 pieds en dessous de la surface rectangulaire, voilà vous avez une table !");
        }
    }
}
