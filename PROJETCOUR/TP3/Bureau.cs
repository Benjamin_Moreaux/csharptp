﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP3
{
    public class Bureau : Meuble
    {
        public Bureau(uint largeur, uint longueur, uint hauteur)
        {
            this.largeur = largeur;
            this.longueur = longueur;
            this.hauteur = hauteur;
        }

        public override void NoticeDeMontage()
        {
            Console.WriteLine("Mettre la grande planche sur les côtes horizontalement puis les pieds et vous avez un bureau");
        }
    }
}
