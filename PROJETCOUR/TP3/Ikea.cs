﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP3
{
    public class Ikea
    {
        protected string secteur { get; set; }
        List<Meuble> catalogue = new List<Meuble>();

        public Ikea(string secteur)
        {
            this.secteur = secteur;
        }

        public void AjouterMeuble(Meuble meuble)
        {
            catalogue.Add(meuble);
        }

        public void ListerLesProduits()
        {
            if (catalogue.Count > 0)
            {
                Console.WriteLine($"Catalogue de IKEA {secteur} :\n");
                foreach (Meuble meuble in catalogue)
                {
                    Console.WriteLine($"{meuble.ToString()}");
                }
            }
            else
            {
                Console.WriteLine($"Catalogue de IKEA {secteur} :\n");
                Console.WriteLine("Ce ikea n'a pas de meuble a vendre !");
            }
            
        }
    }
}
