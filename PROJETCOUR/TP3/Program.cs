﻿using System;

namespace TP3
{
    class Program
    {
        static void Main(string[] args)
        {
            Bureau monBureau = new Bureau(50,100,80);
            Buffet monBuffet = new Buffet(50,100,80);
            Table maTable = new Table(50,100,80);
            Ikea ikeaParis = new Ikea("Paris");
            Ikea ikeaAmiens = new Ikea("Amiens"); 

            monBuffet.NoticeDeMontage();
            Console.WriteLine(monBuffet.ToString());

            monBureau.NoticeDeMontage();
            Console.WriteLine(monBureau.ToString());

            maTable.NoticeDeMontage();
            Console.WriteLine(maTable.ToString());

            Console.WriteLine("\n------------------------------\n");

            ikeaParis.AjouterMeuble(monBuffet);
            ikeaParis.AjouterMeuble(monBureau);
            ikeaParis.AjouterMeuble(maTable);

            ikeaParis.ListerLesProduits();

            Console.WriteLine("\n------------------------------\n");

            ikeaAmiens.ListerLesProduits();
        }
    }
}
