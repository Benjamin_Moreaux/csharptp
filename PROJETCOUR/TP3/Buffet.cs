﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP3
{
    public class Buffet : Meuble
    {
        public Buffet(uint largeur, uint longueur, uint hauteur)
        {
            this.largeur = largeur;
            this.longueur = longueur;
            this.hauteur = hauteur;
        }

        public override void NoticeDeMontage()
        {
            Console.WriteLine("Assembler les tiroirs, puis la grande planche et les pieds et paf un buffet");
        }
    }
}
