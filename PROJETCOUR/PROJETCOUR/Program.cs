﻿using System;

namespace PROJETCOUR
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            Humain monHumain = new Humain("MOREAUX", "Benjamin", 19);
            Console.WriteLine(monHumain.presentation());

            Voiture maVoiture = new Voiture("Volkswagen", 7, 230, 0, 0, "Bernard");

            Console.WriteLine(maVoiture.presentationpapier());
            maVoiture.accelerer(150);
            maVoiture.accelerer(150);
            maVoiture.freiner(180);
        }
    }
}
