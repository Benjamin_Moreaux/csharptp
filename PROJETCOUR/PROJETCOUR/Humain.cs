﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PROJETCOUR
{
    class Humain
    {
        private string nom { get; set; }
        private string prenom { get; set; }
        private int age { get; set; }

        public Humain(string nom, string prenom, int age)
        {
            this.nom = nom;
            this.prenom = prenom;
            this.age = age;
        }

        public string presentation()
        {
            return $"Bonjour, je m'appelle {nom} {prenom} et j'ai {age} ans !";
        }
    }
}
