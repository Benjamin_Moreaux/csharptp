﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PROJETCOUR
{
    class Voiture
    {
        string constructeur;
        int chevaux_fiscaux;
        uint vitesse_max;
        uint allure;
        int boitedevitesse;
        string conducteur;


        //Constructeur
        public Voiture(string constructeur, int chevaux_fiscaux,uint vitesse_max, uint allure, int boitedevitesse, string conducteur)
        {
            this.constructeur = constructeur;
            this.chevaux_fiscaux = chevaux_fiscaux;
            this.vitesse_max = vitesse_max;
            this.allure = allure;
            this.boitedevitesse = boitedevitesse;
            this.conducteur = conducteur;
        }

        //Méthode
        public void accelerer(uint accelaration)
        {
            allure = allure + accelaration;
            if(allure > vitesse_max) { allure = vitesse_max; }
            changervitesse(allure);
            Console.WriteLine($"J'accelere, vitesse actuel de : {allure} km/h : vitesse : {boitedevitesse}");
        }

        public void freiner(uint deccelaration)
        {
            if((int)allure - deccelaration < 0) { allure = 0; }
            else { allure = allure - deccelaration; }
            changervitesse(allure);
            Console.WriteLine($"Je freine, vitesse actuel de : {allure} km/h : vitesse : {boitedevitesse}");
        }

        public string presentationpapier()
        {
            return $"Conducteur = {conducteur} \nConstructeur = {constructeur} \nCheveux fiscaux = {chevaux_fiscaux}";
        }

        public void changervitesse(uint allure)
        {
            if (allure <= 20) { boitedevitesse = 1; }
            else if (allure <= 40) { boitedevitesse = 2; }
            else if (allure <= 60) { boitedevitesse = 3; }
            else if (allure <= 80) { boitedevitesse = 4; }
            else { boitedevitesse = 5; }
        }

        //Getter - Setter

    }
}
