﻿using System;

namespace RPG
{
    public class Heros
    {
        int pointDeVie;
        int attaque;

        public Heros(int pointDeVie, int attaque)
        {
            this.pointDeVie = pointDeVie;
            this.attaque = attaque;
        }

        public int attaquer(int entiteAttaque)
        {
            entiteAttaque = entiteAttaque - attaque;
            if(entiteAttaque <= 0)
            {
                Console.WriteLine($"Le héros attaque le monstre il perd {attaque} pv, il lui reste 0 pv !");
            }
            else
            {
                Console.WriteLine($"Le héros attaque le monstre il perd {attaque} pv, il lui reste {entiteAttaque} pv!");
            }
            return entiteAttaque;
        }

        public int getPointDeVie()
        {
            return pointDeVie;
        }

        public void setPointDeVie(int pointDeVie)
        {
            this.pointDeVie = pointDeVie;
        }

        public void setDegat(int Degat)
        {
            this.attaque = Degat;
        }

        public int getDegat()
        {
            return attaque;
        }
    }
}
