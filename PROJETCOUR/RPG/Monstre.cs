﻿using System;

namespace RPG
{
    public abstract class Monstre
    {

        int pointDeVie;
        int degatAttaque;

        public int attaquer(int pointDeVieHero)
        {
            pointDeVieHero -= degatAttaque;
            if (pointDeVieHero <= 0)
            {
                Console.WriteLine($"Le monstre attaque le héros, il perd {degatAttaque} points de vie ! Il lui reste 0 pv !"); ;
            }
            else
            {
                Console.WriteLine($"Le monstre attaque le héros, il perd {degatAttaque} points de vie ! Il lui reste {pointDeVieHero} !");
            }
            return pointDeVieHero;
        }

        public int getPointDeVie()
        {
            return pointDeVie;
        }

        public void setPointDeVie(int pointDeVie)
        {
            this.pointDeVie = pointDeVie;
        }

        public void setDegat(int degatAttaque)
        {
            this.degatAttaque = degatAttaque;
        }

        public string getType()
        {
            return $"{GetType().Name}";
        }
    }
}
