﻿using System;

namespace RPG
{
    class Program
    {
        /* Fonction jeu
         * Paramètre : Objet Heros
         * Permet de simuler le jeu en fonction des variables
         */
        static void jeu(Heros unHeros)
        {
            //Initialisation variable
            int monstreVaincu = 0;
            int i = 1;
            Gestionnaire_de_partie partie = new Gestionnaire_de_partie();

            //Affichage des points de vie
            Console.WriteLine($"Vos point de vie = {unHeros.getPointDeVie()}");
            Console.ReadKey();
            Console.Clear();

            //Boucle de jeu
            do
            {
                //Création de monstre et affichage de sa création
                Monstre monstre = partie.ChoixMonstre();
                do
                {
                    //Affichage du tour du héros
                    Console.WriteLine($"Tour n°{i} : Heros \n");

                    //Appel de la fonction tour héros
                    TourHeros(unHeros, monstre);

                    //Vérification des pv du monstre
                    //Si il a 0hp ou moins alors on arrêt le combat
                    if (monstre.getPointDeVie() <= 0)
                    {
                        Console.WriteLine("Le monstre est vaincu ! \n");
                        monstreVaincu++;
                        Console.WriteLine("Combat Fini! \n");
                        Tresor(unHeros);
                        break;
                    }

                    //Affichage du tour du monstre
                    Console.WriteLine($"Tour n°{i} : Monstre \n");

                    //Appel de la fonction tour du monstre
                    TourMonstre(unHeros, monstre);

                    //Vérification des pv du héros
                    //Si il a 0hp ou moins on arrête le combat puis la partie
                    if (unHeros.getPointDeVie() <= 0)
                    {
                        break;
                    }

                    //On incrémente le nombre de tour
                    i++;
                    Console.ReadKey();         
                } while(monstre.getPointDeVie() > 0);
                if (monstreVaincu == 5 && unHeros.getPointDeVie()>0)
                { 
                    Console.WriteLine("Vous allez vous spécialiser !!\n");
                    Console.WriteLine($"{partie.Specialisation(unHeros, unHeros.getPointDeVie(), unHeros.getDegat())}");
                    Console.ReadKey();
                }
                //Gestion de l'affichage
                Console.ReadKey();
                Console.Clear();
                Console.WriteLine($"Vos pv = {unHeros.getPointDeVie()}\n");
                
                //On met la variable de tour i à 1 (on reset)
                i = 1;
            } while (unHeros.getPointDeVie() > 0);

            //Affichage de défaite
            Console.WriteLine($"Le héros est vaincu !!! Vous avez vaincu {monstreVaincu} monstre(s)!\n");
            Console.WriteLine("Vous avez perdu!!!\n");
        }

        /*Fonction TourHeros
         * Paramètre : Objet Heros
         * Paramètre : Objet Monstre
         * Permet de simuler l'attaque du héros lors de son tour
         */
        static void TourHeros(Heros unHero, Monstre monstre)
        {
            monstre.setPointDeVie(unHero.attaquer(monstre.getPointDeVie()));
        }
       
        /*Fonction TourMonstre
         * Paramètre : Objet Heros
         * Paramètre : Objet Monstre
         * Permet de simuler l'attaque du monstre lors de son tour
        */
        static void TourMonstre(Heros unHero, Monstre monstre)
        {
            unHero.setPointDeVie(monstre.attaquer(unHero.getPointDeVie()));
        }

        /*Fonction Tresor
         * Paramètre : Objet Heros
         * Permet aléatoirement de donner une potion ou de tomber dans un piège
         */
        static void Tresor(Heros unHeros)
        {
            Random tresor = new Random();
            int choix = tresor.Next(0, 2);
            Console.WriteLine("Vous avez gagné un trésor :");
            if (choix == 0)
            {
                Console.WriteLine("Vous avez gagné une potion !\n");
                unHeros.setPointDeVie(unHeros.getPointDeVie()+25);
                Console.WriteLine("+25pv\n");
            }else if (choix == 1)
            {
                Console.WriteLine("Vous êtes tombé dans un piège !\n");
                unHeros.setPointDeVie(unHeros.getPointDeVie() - 25);
                Console.WriteLine("-25pv\n");
            }
        }

        static void Main(string[] args)
        {
            Random valeuraleatoire = new Random();
            Heros monHero = new Heros(valeuraleatoire.Next(10,150), valeuraleatoire.Next(10,65));
            jeu(monHero);
        }
    }
}
