﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RPG
{
    public class Gestionnaire_de_partie
    {
        Random aleatoire = new Random();
        Monstre monstre;
        int pourcentVie, pourcentDegat;
        string specialisationJoueur;

        public Monstre ChoixMonstre()
        {
            
            if(aleatoire.Next(0, 3)==1)
            {
                monstre = new Gobelin(aleatoire.Next(15, 35), aleatoire.Next(35,70));
                Console.WriteLine(monstre.GetType() + " apparaît !!");
            }
            else if(aleatoire.Next(0, 3) == 2)
            {
                monstre = new Squellette(aleatoire.Next(50, 105), aleatoire.Next(15, 45));
                Console.WriteLine(monstre.GetType() + " apparaît !!");
            }
            else if(aleatoire.Next(0, 3) == 3)
            {
                monstre = new Sorciere(aleatoire.Next(10, 35), aleatoire.Next(50, 100));
                Console.WriteLine(monstre.GetType() + " apparaît !!");
            }
            else
            {
                monstre = new Gobelin(aleatoire.Next(15, 35), aleatoire.Next(35, 70));
                Console.WriteLine(monstre.GetType() + " apparaît !!");
            }
            return monstre;
        }

        public string Specialisation(Heros heros, int pointDeVie, int degat)
        {
            if (aleatoire.Next(1, 3) == 1)
            {
                pourcentVie = pointDeVie + (pointDeVie * 20 / 100);
                pourcentDegat = degat + (degat * 10 / 100);
                heros.setPointDeVie(pourcentVie);
                heros.setDegat(pourcentDegat);
                specialisationJoueur = "Vous êtes un chevalier !!";
                Console.Clear();
            }
            else if (aleatoire.Next(1, 3) == 2)
            {
                pourcentVie = pointDeVie + (pointDeVie * 15 / 100);
                pourcentDegat = degat + (degat * 15 / 100);
                heros.setPointDeVie(pourcentVie);
                heros.setDegat(pourcentDegat);
                specialisationJoueur = "Vous êtes un archer !!";
                Console.Clear();
            }
            else if (aleatoire.Next(1, 3) == 3)
            {
                pourcentVie = pointDeVie + (pointDeVie * 10 / 100);
                pourcentDegat = degat + (degat * 20 / 100);
                heros.setPointDeVie(pourcentVie);
                heros.setDegat(pourcentDegat);
                specialisationJoueur = "Vous êtes un magicien !!";
                Console.Clear();
            }

            return specialisationJoueur;
        }
    }
}
